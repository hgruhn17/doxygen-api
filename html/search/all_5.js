var searchData=
[
  ['getimage_23',['GetImage',['../classnhs__kiosk__application_1_1_content_1_1_document_send_card.html#a54095a85de2975f1e50da480c751ed10',1,'nhs_kiosk_application::Content::DocumentSendCard']]],
  ['getnumquestions_24',['GetNumQuestions',['../classnhs__kiosk__application_1_1_content_1_1_score_card.html#a775179c46606bb52a520bc0f00b49bfb',1,'nhs_kiosk_application::Content::ScoreCard']]],
  ['getoptions_25',['GetOptions',['../classnhs__kiosk__application_1_1_content_1_1_quiz.html#a02cfa53a4e502659d90b4379c6e0412f',1,'nhs_kiosk_application::Content::Quiz']]],
  ['getpoints_26',['GetPoints',['../classnhs__kiosk__application_1_1_content_1_1_score_card.html#a293f8f200e7deccba17875510fa41288',1,'nhs_kiosk_application::Content::ScoreCard']]],
  ['getquestion_27',['GetQuestion',['../classnhs__kiosk__application_1_1_content_1_1_quiz.html#a5d82c101b1fa615de834ef2f2b2cc992',1,'nhs_kiosk_application::Content::Quiz']]],
  ['getscore_28',['GetScore',['../classnhs__kiosk__application_1_1_content_1_1_score_card.html#abe1d69c390996856651730ca2c042f54',1,'nhs_kiosk_application::Content::ScoreCard']]],
  ['getsolidcolorbrush_29',['GetSolidColorBrush',['../classnhs__kiosk__application_1_1_content_1_1_document_send_card.html#aec886d7f22503b747d8d5ff4925af2b2',1,'nhs_kiosk_application::Content::DocumentSendCard']]]
];
